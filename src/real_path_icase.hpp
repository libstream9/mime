#ifndef STREAM9_XDG_MIME_REAL_PATH_ICASE_HPP
#define STREAM9_XDG_MIME_REAL_PATH_ICASE_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/optional.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::mime {

opt<string>
real_path_icase(cstring_ptr const& path);

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_REAL_PATH_ICASE_HPP
