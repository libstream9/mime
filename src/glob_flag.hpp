#ifndef STREAM9_XDG_MIME_GLOB_FLAG_HPP
#define STREAM9_XDG_MIME_GLOB_FLAG_HPP

#include <cstdint>

#include <stream9/bit_flags.hpp>

namespace stream9::xdg::mime {

enum class glob_flag : uint32_t {
    none = 0,
    case_sensitive = 1,
};

using glob_flags = stream9::bit_flags<glob_flag>;

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_GLOB_FLAG_HPP
