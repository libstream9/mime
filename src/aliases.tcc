#ifndef STREAM9_XDG_MIME_ALIASES_TCC
#define STREAM9_XDG_MIME_ALIASES_TCC

#include "aliases.hpp"

namespace stream9::xdg::mime {

struct alias_list_entry {
    card32_t alias_offset;
    card32_t mime_type_offset;
};

struct alias_list {
    card32_t n_aliases;
    alias_list_entry entries[1];
};

/*
 * alias_entry
 */
inline alias_entry::
alias_entry(char const* const base, alias_list_entry const* const entry) noexcept
    : m_base { base }
    , m_entry { entry }
{}

inline cstring_view alias_entry::
alias() const noexcept
{
    return m_base + m_entry->alias_offset;
}

inline cstring_view alias_entry::
mime_type() const noexcept
{
    return m_base + m_entry->mime_type_offset;
}

template<std::size_t I>
decltype(auto) alias_entry::
get() const noexcept
{
    if constexpr (I == 0)
        return alias();
    else
        return mime_type();
}

/*
 * aliases
 */
inline aliases::
aliases(char const* base, card32_t offset) noexcept
    : m_base { base }
    , m_record { reinterpret_cast<alias_list const*>(m_base + offset) }
{}

inline aliases::const_iterator aliases::
begin() const noexcept
{
    return { m_base, &m_record->entries[0] };
}

inline aliases::const_iterator aliases::
end() const noexcept
{
    return { m_base, &m_record->entries[size()] };
}

inline std::uint32_t aliases::
size() const noexcept
{
    return m_record->n_aliases;
}

/*
 * alias_iterator
 */
inline alias_iterator::
alias_iterator(char const* base, alias_list_entry const* ent) noexcept
    : m_base { base }
    , m_entry { ent }
{}

inline alias_entry alias_iterator::
dereference() const noexcept
{
    return { m_base, m_entry };
}

inline void alias_iterator::
increment() noexcept
{
    ++m_entry;
}

inline void alias_iterator::
decrement() noexcept
{
    --m_entry;
}

inline void alias_iterator::
advance(difference_type n) noexcept
{
    m_entry += n;
}

inline alias_iterator::difference_type alias_iterator::
distance_to(alias_iterator const& other) const noexcept
{
    return other.m_entry - m_entry;
}

inline bool alias_iterator::
equal(alias_iterator const& other) const noexcept
{
    return m_base == other.m_base
        && m_entry == other.m_entry;
}

inline std::strong_ordering alias_iterator::
compare(alias_iterator const& other) const noexcept
{
    if (auto cmp = m_base <=> other.m_base; cmp != 0) {
        return cmp;
    }
    else {
        return m_entry <=> other.m_entry;
    }
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_ALIASES_TCC
