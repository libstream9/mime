#include "magic.hpp"

#include "cache.hpp"
#include "magic_match.hpp"

#include <stream9/xdg/mime/error.hpp>

#include <algorithm>
#include <cstdio>
#include <cstring>
#include <locale>
#include <ostream>
#include <ranges>

#include <stream9/json.hpp>
#include <stream9/push_back.hpp>

namespace stream9::xdg::mime {

/*
 * magics
 */
void magics::
search(array<magic>& result,
       file_data const& data) const
{
    try {
        for (auto const& magic: *this) {
            if (magic.execute(data)) {
                push_back(result, magic);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

bool magics::
has_match(string_view mime_type,
          file_data const& data) const noexcept
{
    for (auto const& magic: *this) {
        if (magic.execute(data) && magic.mime_type() == mime_type) {
            return true;
        }
    }

    return false;
}

/*
 * magic
 */
bool magic::
execute(file_data const& data) const noexcept
{
    return std::ranges::any_of(*this,
        [&](auto&& match) { return match.execute(data); } );
}

bool magic::
has_magic_deleteall() const noexcept
{
    char tag[] = "__NOMAGIC__";

    return execute(tag);
}

static string
value_to_string(match::value_t const& v)
{
    try {
        string result;
        std::locale l;

        for (auto c: v) {
            if (std::isprint(static_cast<char>(c), l)) {
                result.append(static_cast<char>(c));
            }
            else {
                char buf[10];
                std::snprintf(buf, 10, "\\x%02x", c);
                result.append(buf);
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static void
tag_invoke(json::value_from_tag, json::value& jv, match const& m)
{
    try {
        json::object obj {
            { "range_start", m.range_start() },
            { "range_length", m.range_length() },
            { "word_size", m.word_size() },
            { "value", value_to_string(m.value()) },
        };
        auto o_mask = m.mask();
        if (o_mask) {
            obj["mask"] = value_to_string(*o_mask);
        }

        if (m.size() != 0) {
            json::array children;
            for (auto const& c: m) {
                children.push_back(json::value_from(c));
            }
            obj["children"] = children;
        }

        jv = std::move(obj);
    }
    catch (...) {
        rethrow_error();
    }
}

static void
tag_invoke(json::value_from_tag, json::value& jv, magic const& m)
{
    try {
        jv = json::object {
            { "mime_type", m.mime_type() },
            { "priority", m.priority().value() },
        };

        json::array matches;
        for (auto const& n: m) {
            matches.push_back(json::value_from(n));
        }
        jv.get_object()["matches"] = matches;
    }
    catch (...) {
        rethrow_error();
    }
}

std::ostream&
operator<<(std::ostream& os, magic const& m)
{
    try {
        return os << json::value_from(m);
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * match
 */
static bool
masked_equal(auto const& range, auto const& value, auto const& mask) noexcept
{
    for (size_t i = 0; i < value.size(); ++i) {
        if ((range[i] & mask[i]) != (value[i] & mask[i])) return false;
    }
    return true;
}

static bool
probe_range(match const& m, file_data const& data) noexcept
{
    auto range_start = m.range_start();
    auto range_length = m.range_length();
    auto value = m.value();
    auto o_mask = m.mask();

    for (size_t i = 0; i < range_length; ++i) {
        auto start_idx = range_start + i;
        if (start_idx > data.size()) return false;

        array_view range { data, start_idx };
        if (range.size() < value.size()) return false;

        if (!o_mask) {
            if (std::memcmp(range.data(), value.data(), value.size()) == 0) {
                return true;
            }
        }
        else {
            assert(o_mask->size() == value.size());
            if (masked_equal(range, value, *o_mask)) return true;
        }
    }

    return false;
}

bool match::
execute(file_data const& data) const noexcept
{
    if (!probe_range(*this, data)) return false;

    return std::ranges::empty(*this)
        || std::ranges::any_of(*this,
            [&](auto&& child) { return child.execute(data); } );
}

} // namespace stream9::xdg::mime
