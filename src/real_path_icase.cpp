#include "real_path_icase.hpp"

#include <stream9/xdg/mime/error.hpp>

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/directory.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/optional.hpp>
#include <stream9/path/absolute.hpp>
#include <stream9/path/append.hpp> // operator/=
#include <stream9/path/components.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/strings/equal.hpp>

namespace stream9::xdg::mime {

static opt<string_view>
find_name_icase(lx::directory& dir, string_view fname)
{
    for (auto it = dir.begin(); it != dir.end(); ++it) {
        auto n = lx::name(*it);
        if (str::iequal(n, fname)) {
            return n;
        }
    }

    return null;
}

static opt<lx::directory>
open_directory(cstring_ptr const& p)
{
    try {
        return lx::directory(p);
    }
    catch (...) {
        return null;
    }
}

static opt<lx::directory>
open_directory(lx::directory& dir, cstring_ptr const& fname)
{
    try {
        return lx::directory(dir.fd(), fname);
    }
    catch (...) {
        return null;
    }
}

static void
pop_front(auto& seq)
{
    auto i = seq.begin();
    if (i == seq.end()) return;

    seq.erase(i, i+1);
}

static bool
is_directory(lx::directory& dir, string_view fname)
{
    auto o_st = lx::nothrow::stat(dir.fd(), fname);
    return o_st && lx::is_directory(*o_st);
}

opt<string>
real_path_icase(cstring_ptr const& p)
{
    try {
        using path::operator/=;

        if (p == "") return p;

        string result;

        auto o_dir = open_directory(path::is_absolute(p) ? "/" : ".");
        if (!o_dir) return null;

        auto segments = path::components(p);
        auto it = segments.begin();
        auto end = segments.end();
        if (*it == "/") {
            ++it;
            result = "/";
        }

        for (; it != end; ++it) {
            if (!o_dir) return null;

            auto o_name = find_name_icase(*o_dir, *it);
            if (o_name) {
                result /= *o_name;
                if (is_directory(*o_dir, *o_name)) {
                    o_dir = open_directory(*o_dir, *o_name);
                }
                else {
                    o_dir = null;
                }
            }
            else {
                return null;
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime
