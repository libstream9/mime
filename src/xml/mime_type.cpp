#include "mime_type.hpp"

#include "mime_type_impl.hpp"

#include "rapidxml/rapidxml.hpp"

#include "../rapidxml_print.hpp"
#include "../rapidxml_util.hpp"

#include <stream9/xdg/mime/error.hpp>

#include <charconv>
#include <iterator>

#include <stream9/emplace_back.hpp>
#include <stream9/filesystem/load_string.hpp>
#include <stream9/from_string.hpp>
#include <stream9/log.hpp>
#include <stream9/push_back.hpp>

namespace stream9::xdg::mime::xml {

namespace {

static inline cstring_view
get_property(rapidxml::xml_node<>& parent,
             string_view element, string_view attribute)
{
    try {
        if (auto* el = first_element(parent, element); el) {
            auto value = get_attribute(*el, attribute);
            if (value.empty()) {
                log::err() << "attribute " << attribute
                           << " is expected on element << " << element << "\n";
            }
            else {
                return value;
            }
        }

        return "";
    }
    catch (...) {
        rethrow_error();
    }
}

static inline array<cstring_view>
get_properties(rapidxml::xml_node<>& parent,
               string_view element,
               string_view attribute)
{
    try {
        array<cstring_view> result;

        for (auto& node: element_iterator { parent, element }) {
            auto value = get_attribute(node, attribute);
            if (value.empty()) {
                log::err() << "attribute " << attribute
                           << " is expected on element "
                           << element << "\n";
            }
            else {
                push_back(result, value);
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static inline std::uint32_t
to_weight(string_view const s)
{
    try {
        if (s.empty()) return 50;

        return from_string<std::uint32_t>(s);
    }
    catch (...) {
        rethrow_error();
    }
}

static inline void
append_property(rapidxml::memory_pool<>& mp, rapidxml::xml_node<>& parent,
               string_view el_name,
               string_view attr_name,
               string_view attr_value)
{
    try {
        for (auto& node: element_iterator { parent, el_name }) {
            if (get_attribute(node, attr_name) == attr_value) {
                log::err() << "duplicated element, name: " << el_name
                           << ", attr: " << attr_name << "\n";
                return;
            }
        }

        auto& el = create_element(mp, el_name);
        set_attribute(mp, el, attr_name, allocate_string(mp, attr_value));
        append_child(parent, el);
    }
    catch (...) {
        rethrow_error();
    }
}

static inline void
set_property(rapidxml::memory_pool<>& mp, rapidxml::xml_node<>& parent,
             string_view element,
             string_view attribute, string_view value)
{
    try {
        auto* el = first_element(parent, element);
        if (el) {
            set_attribute(mp, *el, attribute, value);
        }
        else {
            auto& el = create_element(mp, element);
            set_attribute(mp, el, attribute, value);
            append_child(parent, el);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static inline cstring_view
to_cstring(rapidxml::memory_pool<>& mp, std::integral auto i)
{
    try {
        char* buf = mp.allocate_string(nullptr, 8);
        auto const& [buf_end, ec] = std::to_chars(buf, buf + 7, i); // last byte is reserved for null
        if (ec != std::errc()) {
            throw_error("std::to_chars()", std::make_error_code(ec));
        }
        *buf_end = '\0';

        return { buf, buf_end };
    }
    catch (...) {
        rethrow_error();
    }
}

} // unnamed namespace

/*
 * mime_type::impl
 */
mime_type::impl::
impl(cstring_ptr const& xml_path)
{
    try {
        m_text = fs::load_string(xml_path);
        m_doc.parse<0>(m_text.data());

        auto* node = first_element(m_doc, "mime-type");
        if (!node) {
            throw_error(errc::parse_error, {
                { "path", xml_path },
                { "description", "no mime-type element" }
            });
        }
    }
    catch (rapidxml::parse_error const& e) {
        throw_error(errc::parse_error, {
            { "path", xml_path },
        });
    }
    catch (...) {
        rethrow_error();
    }
}

rapidxml::xml_node<> const& mime_type::impl::
root() const
{
    try {
        auto* node = first_element(m_doc, "mime-type");
        assert(node);
        return *node;
    }
    catch (...) {
        rethrow_error();
    }
}

rapidxml::xml_node<>& mime_type::impl::
root()
{
    try {
        return const_cast<rapidxml::xml_node<>&>(
            const_cast<impl const*>(this)->root() );
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * mime_type
 */
mime_type::
mime_type(string_view name, string_view comment)
{
    try {
        auto& doc = m_p->doc();
        auto& root = create_element(doc, "mime-type");

        set_attribute(doc, root, "xmlns",
            "http://www.freedesktop.org/standards/shared-mime-info");

        set_attribute(doc, root, "type", allocate_string(doc, name));

        auto& comment_el =
            create_element(doc, "comment", allocate_string(doc, comment));
        append_child(root, comment_el);

        append_child(doc, root);
    }
    catch (...) {
        rethrow_error();
    }
}

mime_type::
mime_type(cstring_ptr const& xml_path)
    try : m_p { xml_path }
{}
catch (...) {
    rethrow_error();
}

mime_type::~mime_type() noexcept = default;

cstring_view mime_type::
name() const
{
    try {
        return get_attribute(m_p->root(), "type");
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view mime_type::
comment(string_view language) const
{
    try {
        return get_text(m_p->root(), "comment", language);
    }
    catch (...) {
        rethrow_error();
    }
}

array<struct mime_type::text> mime_type::
comments() const
{
    try {
        array<struct text> result;

        for (auto const& el: element_iterator { m_p->root(), "comment" }) {
            auto lang = get_attribute(el, "xml:lang");
            auto text = value(el);

            emplace_back(result, text, lang);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<cstring_view> mime_type::
aliases() const
{
    try {
        return get_properties(m_p->root(), "alias", "type");
    }
    catch (...) {
        rethrow_error();
    }
}

array<cstring_view> mime_type::
base_classes() const
{
    try {
        return get_properties(m_p->root(), "sub-class-of", "type");
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view mime_type::
icon() const
{
    try {
        return get_property(m_p->root(), "icon", "name");
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view mime_type::
generic_icon() const
{
    try {
        return get_property(m_p->root(), "generic-icon", "name");
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view mime_type::
acronym(string_view language) const
{
    try {
        return get_text(m_p->root(), "acronym", language);
    }
    catch (...) {
        rethrow_error();
    }
}

array<struct mime_type::text> mime_type::
acronyms() const
{
    try {
        array<struct text> result;

        for (auto const& el: element_iterator { m_p->root(), "acronym" }) {
            auto lang = get_attribute(el, "xml:lang");
            auto text = value(el);

            emplace_back(result, text, lang);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view mime_type::
expanded_acronym(string_view language) const
{
    try {
        return get_text(m_p->root(), "expanded-acronym", language);
    }
    catch (...) {
        rethrow_error();
    }
}

array<struct mime_type::text> mime_type::
expanded_acronyms() const
{
    try {
        array<struct text> result;

        for (auto const& el: element_iterator { m_p->root(), "expanded-acronym" }) {
            auto lang = get_attribute(el, "xml:lang");
            auto text = value(el);

            emplace_back(result, text, lang);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<struct mime_type::glob> mime_type::
globs() const
{
    try {
        array<struct glob> result;

        for (auto& el: element_iterator { m_p->root(), "glob" }) {
            auto pattern = get_attribute(el, "pattern");
            if (pattern.empty()) {
                log::err() << "pattern attribute is expected on glob element\n";
                continue;
            }
            auto weight = get_attribute(el, "weight");

            emplace_back(result, pattern, to_weight(weight));
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static inline array<mime_type::match>
get_matches(rapidxml::xml_node<>& parent)
{
    try {
        array<mime_type::match> result;

        for (auto& el: element_iterator { parent, "match" }) {
            auto type = get_attribute(el, "type");
            auto offset = get_attribute(el, "offset");
            auto value = get_attribute(el, "value");
            auto mask = get_attribute(el, "mask");

            emplace_back(result, type, offset, value, mask, get_matches(el));
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<struct mime_type::magic> mime_type::
magics() const
{
    try {
        array<struct mime_type::magic> result;

        for (auto& el: element_iterator { m_p->root(), "magic" }) {
            auto priority = to_weight(get_attribute(el, "priority"));

            emplace_back(result, priority, get_matches(el));
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static inline array<mime_type::treematch>
get_treematches(rapidxml::xml_node<>& parent)
{
    try {
        array<mime_type::treematch> result;

        for (auto& el: element_iterator { parent, "treematch" }) {
            emplace_back(result,
                get_attribute(el, "path"),
                get_attribute(el, "type"),
                get_attribute(el, "match-case"),
                get_attribute(el, "executable"),
                get_attribute(el, "non-empty"),
                get_attribute(el, "mimetype"),
                get_treematches(el)
            );
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<struct mime_type::treemagic> mime_type::
treemagics() const
{
    try {
        array<struct mime_type::treemagic> result;

        for (auto& el: element_iterator { m_p->root(), "treemagic" }) {
            auto priority = to_weight(get_attribute(el, "priority"));

            emplace_back(result, priority, get_treematches(el));
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<struct mime_type::root_xml> mime_type::
root_xmls() const
{
    try {
        array<struct mime_type::root_xml> result;

        for (auto& el: element_iterator { m_p->root(), "root-XML" }) {
            emplace_back(result,
                get_attribute(el, "namespaceURI"),
                get_attribute(el, "localName")
            );
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

bool mime_type::
glob_deleteall() const
{
    try {
        return first_element(m_p->root(), "glob-deleteall");
    }
    catch (...) {
        rethrow_error();
    }
}

bool mime_type::
magic_deleteall() const
{
    try {
        return first_element(m_p->root(), "magic-deleteall");
    }
    catch (...) {
        rethrow_error();
    }
}

string mime_type::
to_xml() const
{
    try {
        std::string xml;
        rapidxml::print(std::back_inserter(xml), m_p->doc(), 0);
        return xml;
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
set_comment(string_view text, string_view language)
{
    try {
        auto& mp = m_p->doc();

        set_text(mp, m_p->root(), "comment",
                 allocate_string(mp, language), allocate_string(mp, text));
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
append_alias(string_view name)
{
    try {
        append_property(m_p->doc(), m_p->root(), "alias", "type", name);
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
append_base_class(string_view name)
{
    try {
        append_property(m_p->doc(), m_p->root(), "sub-class-of", "type", name);
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
set_icon(string_view name)
{
    try {
        auto& mp = m_p->doc();
        set_property(mp, m_p->root(), "icon", "name", allocate_string(mp, name));
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
set_generic_icon(string_view name)
{
    try {
        auto& mp = m_p->doc();
        set_property(mp, m_p->root(), "generic-icon", "name", allocate_string(mp, name));
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
set_acronym(string_view text, string_view language)
{
    try {
        auto& mp = m_p->doc();

        set_text(mp, m_p->root(), "acronym",
                 allocate_string(mp, language), allocate_string(mp, text));
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
set_expanded_acronym(string_view text, string_view language)
{
    try {
        auto& mp = m_p->doc();

        set_text(mp, m_p->root(), "expanded-acronym",
                 allocate_string(mp, language), allocate_string(mp, text));
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
append_glob(string_view pattern, std::uint32_t weight/*= 50*/)
{
    try {
        auto& parent = m_p->root();
        auto& mp = m_p->doc();

        auto& el = create_element(mp, "glob");
        set_attribute(mp, el, "pattern", allocate_string(mp, pattern));
        if (weight != 50) {
            set_attribute(mp, el, "weight", to_cstring(mp, weight));
        }
        append_child(parent, el);
    }
    catch (...) {
        rethrow_error();
    }
}

static inline void
append_matches(rapidxml::memory_pool<>& mp, rapidxml::xml_node<>& parent, auto& matches)
{
    try {
        for (auto const& match: matches) {
            auto& el = create_element(mp, "match");
            set_attribute(mp, el, "type", allocate_string(mp, match.type));
            set_attribute(mp, el, "offset", allocate_string(mp, match.offset));
            set_attribute(mp, el, "value", allocate_string(mp, match.value));
            if (!match.mask.empty()) {
                set_attribute(mp, el, "mask", allocate_string(mp, match.mask));
            }

            append_matches(mp, el, match.children);

            append_child(parent, el);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
append_magic(struct magic const& magic)
{
    try {
        auto& parent = m_p->root();
        auto& mp = m_p->doc();

        auto& el = create_element(mp, "magic");
        if (magic.priority != 50) {
            set_attribute(mp, el, "priority", to_cstring(mp, magic.priority));
        }
        append_matches(mp, el, magic.matches);

        append_child(parent, el);
    }
    catch (...) {
        rethrow_error();
    }
}

static inline void
append_treematches(rapidxml::memory_pool<>& mp, rapidxml::xml_node<>& parent, auto& matches)
{
    try {
        for (auto const& match: matches) {
            auto& el = create_element(mp, "treematch");
            set_attribute(mp, el, "path", allocate_string(mp, match.path));
            set_attribute(mp, el, "type", allocate_string(mp, match.type));
            set_attribute(mp, el, "match-case", allocate_string(mp, match.match_case));
            set_attribute(mp, el, "executable", allocate_string(mp, match.executable));
            set_attribute(mp, el, "non-empty", allocate_string(mp, match.non_empty));
            set_attribute(mp, el, "mimetype", allocate_string(mp, match.mime_type));

            append_treematches(mp, el, match.children);

            append_child(parent, el);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
append_treemagic(struct treemagic const& magic)
{
    try {
        auto& parent = m_p->root();
        auto& mp = m_p->doc();

        auto& el = create_element(mp, "treemagic");
        if (magic.priority != 50) {
            set_attribute(mp, el, "priority", to_cstring(mp, magic.priority));
        }
        append_treematches(mp, el, magic.matches);

        append_child(parent, el);
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
append_root_xml(string_view namespace_uri, string local_name)
{
    try {
        auto& parent = m_p->root();
        auto& mp = m_p->doc();

        auto& el = create_element(mp, "root-XML");
        set_attribute(mp, el, "namespaceURI", allocate_string(mp, namespace_uri));
        set_attribute(mp, el, "localName", allocate_string(mp, local_name));

        append_child(parent, el);
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
set_glob_deleteall(bool flag/*= true*/)
{
    try {
        auto& root = m_p->root();
        auto& mp = m_p->doc();
        auto name = "glob-deleteall";

        auto* el = first_element(root, name);
        if (el) {
            if (!flag) root.remove_node(el);
        }
        else {
            if (flag) {
                auto& el = create_element(mp, name);
                append_child(root, el);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
set_magic_deleteall(bool flag/*= true*/)
{
    try {
        auto& root = m_p->root();
        auto& mp = m_p->doc();
        auto name = "magic-deleteall";

        auto* el = first_element(root, name);
        if (el) {
            if (!flag) root.remove_node(el);
        }
        else {
            if (flag) {
                auto& el = create_element(mp, name);
                append_child(root, el);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static void
merge_text_by_language(rapidxml::memory_pool<>& mp,
              rapidxml::xml_node<>& src, rapidxml::xml_node<>& dest,
              string_view el_name)
{
    try {
        auto attr_name = "xml:lang";

        for (auto const& src_el: element_iterator { src, el_name }) {
            auto lang = get_attribute(src_el, attr_name);
            auto text = value(src_el);

            auto* dest_el = first_element(dest, el_name, attr_name, lang);
            if (dest_el) {
                if (text != value(*dest_el)) {
                    set_value(*dest_el, allocate_string(mp, text));
                }
            }
            else {
                auto& new_el = create_element(mp, el_name);
                set_attribute(mp, new_el, attr_name, allocate_string(mp, lang));
                set_value(new_el, allocate_string(mp, text));

                append_child(dest, new_el);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static void
merge_comment(rapidxml::memory_pool<>& mp,
              rapidxml::xml_node<>& src, rapidxml::xml_node<>& dest)
{
    try {
        merge_text_by_language(mp, src, dest, "comment");
    }
    catch (...) {
        rethrow_error();
    }
}

static void
merge_by_append_if_there_is_no_duplication(
    rapidxml::memory_pool<>& mp,
    rapidxml::xml_node<>& src, rapidxml::xml_node<>& dest,
    string_view el_name,
    string_view attr_name)
{
    try {
        for (auto const& src_el: element_iterator { src, el_name }) {
            auto attr_value = get_attribute(src_el, attr_name);

            auto* dest_el = first_element(dest, el_name, attr_name, attr_value);
            if (!dest_el) {
                auto& new_el = create_element(mp, el_name);
                set_attribute(mp, new_el, attr_name, allocate_string(mp, attr_value));

                append_child(dest, new_el);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static void
merge_alias(rapidxml::memory_pool<>& mp,
            rapidxml::xml_node<>& src, rapidxml::xml_node<>& dest)
{
    try {
        merge_by_append_if_there_is_no_duplication(mp, src, dest, "alias", "type");
    }
    catch (...) {
        rethrow_error();
    }
}

static void
merge_base_class(rapidxml::memory_pool<>& mp,
                 rapidxml::xml_node<>& src, rapidxml::xml_node<>& dest)
{
    try {
        merge_by_append_if_there_is_no_duplication(
                                         mp, src, dest, "sub-class-of", "type");
    }
    catch (...) {
        rethrow_error();
    }
}

static void
merge_by_update_or_append(rapidxml::memory_pool<>& mp,
        rapidxml::xml_node<>& src, rapidxml::xml_node<>& dest,
        string_view el_name, string_view attr_name)
{
    try {
        for (auto const& src_el: element_iterator { src, el_name }) {
            auto attr_value = get_attribute(src_el, attr_name);

            auto* dest_el = first_element(dest, el_name);
            if (dest_el) {
                auto* attr = first_attribute(*dest_el, attr_name);
                if (attr) {
                    if (value(*attr) != attr_value) {
                        set_value(*attr, allocate_string(mp, attr_value));
                    }
                }
                else {
                    auto& new_attr = create_attribute(mp, attr_name, attr_value);
                    dest_el->append_attribute(&new_attr);
                }
            }
            else {
                auto& new_el = create_element(mp, el_name);
                set_attribute(mp, new_el, attr_name, allocate_string(mp, attr_value));

                append_child(dest, new_el);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static void
merge_icon(rapidxml::memory_pool<>& mp,
           rapidxml::xml_node<>& src, rapidxml::xml_node<>& dest)
{
    try {
        merge_by_update_or_append(mp, src, dest, "icon", "name");
    }
    catch (...) {
        rethrow_error();
    }
}

static void
merge_generic_icon(rapidxml::memory_pool<>& mp,
                   rapidxml::xml_node<>& src, rapidxml::xml_node<>& dest)
{
    try {
        merge_by_update_or_append(mp, src, dest, "generic-icon", "name");
    }
    catch (...) {
        rethrow_error();
    }
}

static void
merge_acronym(rapidxml::memory_pool<>& mp,
              rapidxml::xml_node<>& src, rapidxml::xml_node<>& dest)
{
    try {
        merge_text_by_language(mp, src, dest, "acronym");
    }
    catch (...) {
        rethrow_error();
    }
}

static void
merge_expanded_acronym(rapidxml::memory_pool<>& mp,
        rapidxml::xml_node<>& src, rapidxml::xml_node<>& dest)
{
    try {
        merge_text_by_language(mp, src, dest, "expanded-acronym");
    }
    catch (...) {
        rethrow_error();
    }
}

static void
merge_glob(rapidxml::memory_pool<>& mp,
           rapidxml::xml_node<>& src, rapidxml::xml_node<>& dest)
{
    try {
        auto el_name = "glob";
        auto pattern_name = "pattern";
        auto weight_name = "weight";

        if (first_element(src, "glob-deleteall")) {
            auto* el = first_element(dest, el_name);
            while (el) {
                auto* next = next_sibling(*el, el_name);
                dest.remove_node(el);
                el = next;
            }
        }

        for (auto const& src_el: element_iterator { src, el_name }) {
            auto pattern = get_attribute(src_el, pattern_name);
            auto weight = get_attribute(src_el, weight_name);

            auto* dest_el = first_element(dest, el_name, pattern_name, pattern);
            if (dest_el) {
                auto* attr = first_attribute(*dest_el, weight_name);
                if (weight.empty()) {
                    if (attr) {
                        dest_el->remove_attribute(attr);
                    }
                }
                else {
                    if (attr) {
                        if (value(*attr) != weight) {
                            set_value(*attr, allocate_string(mp, weight));
                        }
                    }
                    else {
                        auto& new_attr = create_attribute(
                                    mp, weight_name, allocate_string(mp, weight));
                        dest_el->append_attribute(&new_attr);
                    }
                }
            }
            else {
                auto& new_el = create_element(mp, el_name);

                auto& pattern_attr = create_attribute(
                                  mp, pattern_name, allocate_string(mp, pattern));
                new_el.append_attribute(&pattern_attr);

                auto& weight_attr = create_attribute(
                                    mp, weight_name, allocate_string(mp, weight));
                new_el.append_attribute(&weight_attr);

                append_child(dest, new_el);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_type::
merge(mime_type const& other)
{
    try {
        assert(other.name() == name() && "can't merge different mime type");

        auto& mp = m_p->doc();
        auto& src_root = other.m_p->root();
        auto& dest_root = m_p->root();

        merge_comment(mp, src_root, dest_root);
        merge_alias(mp, src_root, dest_root);
        merge_base_class(mp, src_root, dest_root);
        merge_icon(mp, src_root, dest_root);
        merge_generic_icon(mp, src_root, dest_root);
        merge_acronym(mp, src_root, dest_root);
        merge_expanded_acronym(mp, src_root, dest_root);
        merge_glob(mp, src_root, dest_root);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime::xml
