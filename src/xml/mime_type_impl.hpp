#ifndef STREAM9_XDG_MIME_XML_MIME_TYPE_IMPL_HPP
#define STREAM9_XDG_MIME_XML_MIME_TYPE_IMPL_HPP

#include "mime_type.hpp"

#include "rapidxml/rapidxml.hpp"

#include <stream9/string.hpp>

namespace stream9::xdg::mime::xml {

class mime_type::impl
{
public:
    impl() = default;

    impl(cstring_ptr const& xml_path);

    auto const& doc() const { return m_doc; }
    auto& doc() { return m_doc; }

    rapidxml::xml_node<> const& root() const;
    rapidxml::xml_node<>& root();

private:
    string m_text;
    rapidxml::xml_document<> m_doc;
};

} // namespace stream9::xdg::mime::xml

#endif // STREAM9_XDG_MIME_XML_MIME_TYPE_IMPL_HPP
