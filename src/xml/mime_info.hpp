#ifndef STREAM9_XDG_MIME_XML_MIME_INFO_HPP
#define STREAM9_XDG_MIME_XML_MIME_INFO_HPP

#include <cstddef>
#include <iosfwd>

#include <stream9/node.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::mime::xml {

class mime_type;

class mime_info
{
public:
    // essential
    mime_info() noexcept;

    mime_info(mime_info&&) noexcept;
    mime_info& operator=(mime_info&&) noexcept;

    ~mime_info() noexcept;

    // query
    std::size_t size() const;

    // modifier
    void push_back(mime_type const&);

    // conversion
    string to_xml() const;

private:
    class impl;
    node<impl> m_p;
};

std::ostream& operator<<(std::ostream&, mime_info const&);

} // namespace stream9::xdg::mime::xml

#endif // STREAM9_XDG_MIME_XML_MIME_INFO_HPP
