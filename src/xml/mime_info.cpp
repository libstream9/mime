#include "mime_info.hpp"

#include "mime_type_impl.hpp"

#include "rapidxml/rapidxml.hpp"

#include "../rapidxml_print.hpp"
#include "../rapidxml_util.hpp"

#include <stream9/xdg/mime/error.hpp>

#include <iterator>
#include <ostream>

namespace stream9::xdg::mime::xml {

class mime_info::impl
{
public:
    impl()
    {
        try {
            auto& root = create_element(m_doc, "mime-info");

            set_attribute(m_doc, root, "xmlns",
                "http://www.freedesktop.org/standards/shared-mime-info");

            append_child(m_doc, root);
        }
        catch (...) {
            rethrow_error();
        }
    }

    auto const& doc() const noexcept { return m_doc; }
    auto& doc() noexcept { return m_doc; }

    auto const& root() const
    {
        try {
            auto* node = first_element(m_doc, "mime-info");
            assert(node);
            return *node;
        }
        catch (...) {
            rethrow_error();
        }
    }

    auto& root()
    {
        try {
            auto* node = first_element(m_doc, "mime-info");
            assert(node);
            return *node;
        }
        catch (...) {
            rethrow_error();
        }
    }

private:
    rapidxml::xml_document<> m_doc;
};

mime_info::mime_info() noexcept = default;

mime_info::mime_info(mime_info&&) noexcept = default;
mime_info& mime_info::operator=(mime_info&&) noexcept = default;

mime_info::~mime_info() noexcept = default;

std::size_t mime_info::
size() const
{
    try {
        auto& root = m_p->root();
        size_t cnt = 0;

        for (auto const& el: element_iterator { root, "mime-type" }) {
            (void)el;
            ++cnt;
        }

        return cnt;
    }
    catch (...) {
        rethrow_error();
    }
}

void mime_info::
push_back(mime_type const& mt)
{
    try {
        auto& mp = m_p->doc();
        auto& root = m_p->root();

        rapidxml::xml_document<> tmp;
        tmp.parse<0>(const_cast<char*>(allocate_string(mp, mt.to_xml()).c_str()));

        auto* const node = first_element(tmp, "mime-type");
        assert(node);
        auto* const cloned = mp.clone_node(node);
        auto* const attr = first_attribute(*cloned, "xmlns");
        if (attr) {
            cloned->remove_attribute(attr);
        }

        append_child(root, *cloned);
    }
    catch (...) {
        rethrow_error();
    }
}

string mime_info::
to_xml() const
{
    try {
        std::string xml;
        rapidxml::print(std::back_inserter(xml), m_p->doc(), 0);
        return xml;
    }
    catch (...) {
        rethrow_error();
    }
}

std::ostream&
operator<<(std::ostream& os, mime_info const& info)
{
    try {
        return os << info.to_xml();
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime::xml
