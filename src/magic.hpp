#ifndef STREAM9_XDG_MIME_MAGIC_HPP
#define STREAM9_XDG_MIME_MAGIC_HPP

#include "magic_priority.hpp"

#include <stream9/xdg/mime/file_data.hpp>
#include <stream9/xdg/mime/namespace.hpp>

#include <compare>
#include <cstdint>
#include <iosfwd>
#include <iterator>
#include <ranges>

#include <stream9/array.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/iterators.hpp>
#include <stream9/optional.hpp>
#include <stream9/ranges/range_facade.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::mime {

struct magic_list;
struct match_entry;
struct matchlet_entry;

class cache;
class directory;
class magic;
class magic_iterator;
class match_iterator;

class magics : public stream9::ranges::range_facade<magics>
{
public:
    using const_iterator = magic_iterator;

public:
    magics(class cache const&, char const* base, std::uint32_t offset) noexcept;

    // accessor
    class cache const& cache() const noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // query
    std::uint32_t size() const noexcept;
    std::uint32_t max_extent() const noexcept;

    void search(array<magic>& result, file_data const&) const;

    bool has_match(string_view mime_type, file_data const&) const noexcept;

private:
    class cache const* m_cache; // non-null
    char const* m_base; // non-null
    magic_list const* m_record; // non-null
};

class magic : public stream9::ranges::range_facade<magic>
{
public:
    using const_iterator = match_iterator;

public:
    magic(char const* base, match_entry const*) noexcept;

    // accessor
    magic_priority priority() const noexcept;
    cstring_view mime_type() const noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // query
    std::uint32_t size() const noexcept;

    bool execute(file_data const& data) const noexcept;

    bool has_magic_deleteall() const noexcept;

private:
    char const* m_base; // non-null
    match_entry const* m_record; // non-null
};

std::ostream& operator<<(std::ostream&, magic const&);

class match : public stream9::ranges::range_facade<match>
{
public:
    using value_t = file_data;
    using const_iterator = match_iterator;

public:
    match(char const* base, matchlet_entry const*) noexcept;

    // accessor
    std::uint32_t range_start() const noexcept;
    std::uint32_t range_length() const noexcept;
    std::uint32_t word_size() const noexcept;
    value_t value() const noexcept;
    opt<value_t> mask() const noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // query
    std::uint32_t size() const noexcept;

    bool execute(file_data const& data) const noexcept;

private:
    char const* m_base; // non-null
    matchlet_entry const* m_record; // non-null
};

class magic_iterator : public iter::iterator_facade<
                                            magic_iterator,
                                            std::random_access_iterator_tag,
                                            magic,
                                            magic >
{
public:
    magic_iterator() = default;
    magic_iterator(char const* base, match_entry const*) noexcept;

private:
    friend class iter::iterator_core_access;

    magic dereference() const noexcept;

    void increment() noexcept;

    void decrement() noexcept;

    void advance(difference_type) noexcept;

    difference_type distance_to(magic_iterator const&) const noexcept;

    bool equal(magic_iterator const&) const noexcept;

    std::strong_ordering compare(magic_iterator const&) const noexcept;

private:
    char const* m_base = nullptr;
    match_entry const* m_entry = nullptr;
};

class match_iterator : public iter::iterator_facade<
                                            match_iterator,
                                            std::random_access_iterator_tag,
                                            match,
                                            match >
{
public:
    match_iterator() = default;
    match_iterator(char const* base, matchlet_entry const*) noexcept;

private:
    friend class iter::iterator_core_access;

    match dereference() const noexcept;

    void increment() noexcept;

    void decrement() noexcept;

    void advance(difference_type) noexcept;

    difference_type distance_to(match_iterator const&) const noexcept;

    bool equal(match_iterator const&) const noexcept;

    std::strong_ordering compare(match_iterator const&) const noexcept;

private:
    char const* m_base = nullptr;
    matchlet_entry const* m_entry = nullptr;
};

} // namespace stream9::xdg::mime

#include "magic.tcc"

namespace std::ranges {

template<>
inline constexpr bool enable_borrowed_range<
                                stream9::xdg::mime::magics> = true;

template<>
inline constexpr bool enable_borrowed_range<
                                stream9::xdg::mime::magic> = true;

template<>
inline constexpr bool enable_borrowed_range<
                                stream9::xdg::mime::match> = true;

} // namespace std::ranges

#endif // STREAM9_XDG_MIME_MAGIC_HPP
