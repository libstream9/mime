#ifndef STREAM9_XDG_MIME_REVERSE_SUFFIX_TREE_TCC
#define STREAM9_XDG_MIME_REVERSE_SUFFIX_TREE_TCC

#include "reverse_suffix_tree.hpp"

#include "endian.hpp"

namespace stream9::xdg::mime {

struct tree_entry {
    card32_t n_roots;
    card32_t first_root_offset;
};

struct tree_node_entry {
    card32_t type; // 0 if it is a leaf, otherwise branch
    card32_t filler1;
    card32_t filler2;
};

struct tree_branch_node_entry {
    card32_t character;
    card32_t n_children;
    card32_t first_child_offset;
};

struct tree_leaf_node_entry {
    card32_t zero;
    card32_t mime_type_offset;
    card24_t flags;
    card8_t weight;
};

/*
 * suffix_tree
 */
inline suffix_tree::
suffix_tree(class cache const& c, char const* base, std::uint32_t offset) noexcept
    : m_cache { &c }
    , m_base { base }
    , m_entry { reinterpret_cast<tree_entry const*>(m_base + offset) }
{}

inline class cache const& suffix_tree::
cache() const noexcept
{
    return *m_cache;
}

inline suffix_tree::const_iterator suffix_tree::
begin() const noexcept
{
    auto* entries = reinterpret_cast<tree_node_entry const*>(
                                        m_base + m_entry->first_root_offset );
    return { m_base, &entries[0] };
}

inline suffix_tree::const_iterator suffix_tree::
end() const noexcept
{
    auto* entries = reinterpret_cast<tree_node_entry const*>(
                                        m_base + m_entry->first_root_offset );
    return { m_base, &entries[size()] };
}

inline std::uint32_t suffix_tree::
size() const noexcept
{
    return m_entry->n_roots;
}

/*
 * tree_leaf_node
 */
inline tree_leaf_node::
tree_leaf_node(char const* base, tree_node_entry const* entry) noexcept
    : m_base { base }
    , m_entry { reinterpret_cast<tree_leaf_node_entry const*>(entry) }
{}

inline str::cstring_view tree_leaf_node::
mime_type() const noexcept
{
    return m_base + m_entry->mime_type_offset;
}

inline glob_flags tree_leaf_node::
flags() const noexcept
{
    std::uint32_t flags = m_entry->flags;
    return static_cast<glob_flag>(flags);
}

inline glob_weight tree_leaf_node::
weight() const noexcept
{
    return m_entry->weight.value();
}

/*
 * tree_branch_node
 */
inline tree_branch_node::
tree_branch_node(char const* base, tree_node_entry const* entry) noexcept
    : m_base { base }
    , m_entry { reinterpret_cast<tree_branch_node_entry const*>(entry) }
{}

inline char tree_branch_node::
character() const noexcept
{
    return static_cast<char>(m_entry->character);
}

inline tree_branch_node::const_iterator tree_branch_node::
begin() const noexcept
{
    auto* entries = reinterpret_cast<tree_node_entry const*>(
                                        m_base + m_entry->first_child_offset );
    return { m_base, &entries[0] };
}
inline tree_branch_node::const_iterator tree_branch_node::
end() const noexcept
{
    auto* entries = reinterpret_cast<tree_node_entry const*>(
                                        m_base + m_entry->first_child_offset );
    return { m_base, &entries[size()] };
}

inline std::uint32_t tree_branch_node::
size() const noexcept
{
    return m_entry->n_children;
}

/*
 * tree_node_iterator
 */
inline tree_node_iterator::
tree_node_iterator(char const* base, tree_node_entry const* entry) noexcept
    : m_base { base }
    , m_entry { entry }
{}

inline tree_node tree_node_iterator::
dereference() const noexcept
{
    if (m_entry->type == 0) {
        return tree_leaf_node { m_base, m_entry };
    }
    else {
        return tree_branch_node { m_base, m_entry };
    }
}

inline void tree_node_iterator::
increment() noexcept
{
    ++m_entry;
}

inline void tree_node_iterator::
decrement() noexcept
{
    --m_entry;
}

inline void tree_node_iterator::
advance(difference_type n) noexcept
{
    m_entry += n;
}

inline tree_node_iterator::difference_type tree_node_iterator::
distance_to(tree_node_iterator const& other) const noexcept
{
    return other.m_entry - m_entry;
}

inline bool tree_node_iterator::
equal(tree_node_iterator const& other) const noexcept
{
    return m_base == other.m_base
        && m_entry == other.m_entry;
}

inline std::strong_ordering tree_node_iterator::
compare(tree_node_iterator const& other) const noexcept
{
    if (auto cmp = m_base <=> other.m_base; cmp != 0) {
        return cmp;
    }
    else {
        return m_entry <=> other.m_entry;
    }
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_REVERSE_SUFFIX_TREE_TCC
