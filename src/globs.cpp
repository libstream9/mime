#include "globs.hpp"

#include "directory.hpp"

#include <stream9/xdg/mime/error.hpp>

#include <algorithm>
#include <ostream>
#include <system_error>

#include <stream9/emplace_back.hpp>
#include <stream9/strings/query/lexicographical_compare.hpp>

namespace stream9::xdg::mime {

static char const*
get_pattern(glob const& g) noexcept
{
    return g.pattern();
}

static void
push_match(array<glob_record>& matches, glob const& g)
{
    try {
        emplace_back(matches,
            string { g.pattern() },
            g.mime_type(),
            g.flags(),
            g.weight()
        );
    }
    catch (...) {
        rethrow_error();
    }
}

void globs::
literal_search(array<glob_record>& result,
               string_view filename) const
{
    using std::ranges::equal_range;

    try {
        auto icase_matches = equal_range(
            *this, filename, str::ilexicographical_compare, get_pattern);

        for (auto const& match: icase_matches) {
            if (!(match.flags() & glob_flag::case_sensitive)) {
                push_match(result, match);
            }
        }

        auto matches = equal_range(*this, filename, {}, get_pattern);

        for (auto const& match: matches) {
            if (match.flags() & glob_flag::case_sensitive) {
                push_match(result, match);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void globs::
fnmatch_search(array<glob_record>& result,
               cstring_ptr const& filename) const
{
    try {
        for (auto const& glob: *this) {
            bool icase = false;

            if (!(glob.flags() & glob_flag::case_sensitive)) {
                icase = true;
            }

            if (env().match_filename(glob.pattern(), filename, !icase)) {
                push_match(result, glob);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

bool globs::
has_match(string_view mime_type, string_view pattern) const noexcept
{
    return std::ranges::any_of(*this,
        [&](auto&& g) {
            return g.pattern() == pattern && g.mime_type() == mime_type;
        });
}

std::ostream&
operator<<(std::ostream& os, glob const& g)
{
    try {
        os << g.weight() << ":" << g.mime_type() << ":" << g.pattern();
        if (g.flags() & glob_flag::case_sensitive) {
            os << ":cs";
        }
        return os;
    }
    catch (...) {
        rethrow_error();
    }
}

std::ostream&
operator<<(std::ostream& os, globs const& globs)
{
    try {
        for (auto const& glob: globs) {
            os << glob << "\n";
        }
        return os;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime
