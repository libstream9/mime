#ifndef STREAM9_XDG_MIME_CACHE_HPP
#define STREAM9_XDG_MIME_CACHE_HPP

#include <stream9/xdg/mime/error.hpp>
#include <stream9/xdg/mime/file_data.hpp>
#include <stream9/xdg/mime/namespace.hpp>

#include <cstdint>

#include <stream9/array.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/node.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::mime {

class aliases;
class directory;
class glob_record;
class icons;
class magic;
class magics;
class subclasses;
class suffix_tree;
class xml_namespaces;

class cache
{
public:
    cache(string_view dir_path);
    ~cache() noexcept;

    // accessor
    cstring_view path() const noexcept;

    std::uint16_t major_version() const noexcept;
    std::uint16_t minor_version() const noexcept;

    class aliases aliases() const noexcept;
    class subclasses subclasses() const noexcept;

    class globs globs() const noexcept;
    class globs literals() const noexcept;
    class suffix_tree reverse_suffix_tree() const noexcept;

    class magics magics() const noexcept;

    class xml_namespaces xml_namespaces() const noexcept;

    class icons icons() const noexcept;
    class icons generic_icons() const noexcept;

    // query
    void glob_search(array<glob_record>&,
                     string_view filename) const;

    void magic_search(array<magic>& result,
                      file_data const&) const;

    std::uint32_t magic_data_max_extent() const;

    bool has_glob_deleteall(string_view mime_type) const noexcept;
    bool has_magic_deleteall(string_view mime_type) const noexcept;

    // modifier
    void reload();

private:
    class impl;
    node<impl> m_p;
};

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_CACHE_HPP
