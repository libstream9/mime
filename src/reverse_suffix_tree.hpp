#ifndef STREAM9_XDG_MIME_REVERSE_SUFFIX_TREE_HPP
#define STREAM9_XDG_MIME_REVERSE_SUFFIX_TREE_HPP

#include <stream9/xdg/mime/namespace.hpp>

#include "glob_flag.hpp"
#include "glob_weight.hpp"

#include <cstdint>
#include <compare>
#include <iterator>
#include <variant>

#include <stream9/array.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/iterators.hpp>
#include <stream9/ranges/range_facade.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::mime {

struct tree_entry;
struct tree_branch_node_entry;
struct tree_leaf_node_entry;
struct tree_node_entry;

class cache;
class directory;
class glob_record;
class tree_leaf_node;
class tree_node_iterator;

class suffix_tree : public stream9::ranges::range_facade<suffix_tree>
{
public:
    using const_iterator = tree_node_iterator;

public:
    suffix_tree(class cache const&, char const* base, std::uint32_t offset) noexcept;

    // accessor
    class cache const& cache() const noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // query
    std::uint32_t size() const noexcept;

    void search(array<glob_record>&, string_view filename) const;

private:
    class cache const* m_cache; // non-null
    char const* m_base; // non-null
    tree_entry const* m_entry; // non-null
};

class tree_leaf_node
{
public:
    tree_leaf_node(char const* base, tree_node_entry const*) noexcept;

    // accessor
    cstring_view mime_type() const noexcept;
    glob_flags flags() const noexcept;
    glob_weight weight() const noexcept;

private:
    char const* m_base; // non-null
    tree_leaf_node_entry const* m_entry; // non-null
};

class tree_branch_node
                : public stream9::ranges::range_facade<tree_branch_node>
{
public:
    using const_iterator = tree_node_iterator;

public:
    tree_branch_node(char const* base, tree_node_entry const*) noexcept;

    // accessor
    char character() const noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // query
    std::uint32_t size() const noexcept;

private:
    char const* m_base; // non-null
    tree_branch_node_entry const* m_entry; // non-null
};

using tree_node = std::variant<tree_leaf_node, tree_branch_node>;

class tree_node_iterator : public iter::iterator_facade<tree_node_iterator,
                                            std::random_access_iterator_tag,
                                            tree_node,
                                            tree_node >
{
public:
    tree_node_iterator() = default;
    tree_node_iterator(char const* base, tree_node_entry const*) noexcept;

private:
    friend class iter::iterator_core_access;

    tree_node dereference() const noexcept;

    void increment() noexcept;

    void decrement() noexcept;

    void advance(difference_type) noexcept;

    difference_type distance_to(tree_node_iterator const&) const noexcept;

    bool equal(tree_node_iterator const&) const noexcept;

    std::strong_ordering compare(tree_node_iterator const&) const noexcept;

private:
    char const* m_base = nullptr;
    tree_node_entry const* m_entry = nullptr;
};

} // namespace stream9::xdg::mime

#include "reverse_suffix_tree.tcc"

namespace std::ranges {

template<>
inline constexpr bool enable_borrowed_range<stream9::xdg::mime::suffix_tree> = true;

template<>
inline constexpr bool enable_borrowed_range<stream9::xdg::mime::tree_branch_node> = true;

} // namespace std::ranges

#endif // STREAM9_XDG_MIME_REVERSE_SUFFIX_TREE_HPP
