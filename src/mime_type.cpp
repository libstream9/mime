#include <stream9/xdg/mime/mime_type.hpp>

#include <stream9/xdg/mime/namespace.hpp>
#include <stream9/xdg/mime/database.hpp>

#include "database_impl.hpp"
#include "directory.hpp"
#include "icons.hpp"
#include "subclasses.hpp"
#include "xml/mime_type.hpp"

#include <algorithm>

#include <stream9/emplace_back.hpp>
#include <stream9/optional.hpp>
#include <stream9/push_back.hpp>
#include <stream9/ranges/find.hpp>
#include <stream9/strings/find_first.hpp>
#include <stream9/strings/modifier/find_replace_all.hpp>

namespace stream9::xdg::mime {

class mime_type::impl
{
public:
    impl(database const& db, string_view name)
        try : m_db { &db }
        , m_name { name }
    {}
    catch (...) {
        rethrow_error();
    }

    auto const& db() const noexcept { return *m_db; }
    auto const& dirs() const noexcept { return m_db->m_p->dirs(); }
    auto const& name() const noexcept { return m_name; }

    auto const& xml() const
    {
        try {
            if (!m_xml) {
                for (auto const& dir: this->dirs()) {
                    auto const& xml_path = dir.xml_entry(m_name);
                    if (xml_path.empty()) continue;

                    if (!m_xml) {
                        m_xml.emplace(xml_path);
                    }
                    else {
                        xml::mime_type mt { xml_path };
                        m_xml->merge(mt);
                    }
                }

                if (!m_xml) {
                    m_xml.emplace(m_name, ""); // dummy
                }
            }

            return *m_xml;
        }
        catch (...) {
            rethrow_error();
        }
    }

private:
    database const* m_db; // non-null
    string m_name;
    mutable opt<xml::mime_type> m_xml;
};

/*
 * mime_type
 */
mime_type::
mime_type(database const& db, string_view name)
    try : m_p { db, name }
{}
catch (...) {
    rethrow_error({
        { "name", name }
    });
}

mime_type::mime_type(mime_type&&) noexcept = default;
mime_type& mime_type::operator=(mime_type&&) noexcept = default;

mime_type::~mime_type() = default;

cstring_view mime_type::
name() const noexcept
{
    return m_p->name();
}

cstring_view mime_type::
comment() const
{
    try {
        auto const& xml = m_p->xml();

        return xml.comment("");
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view mime_type::
comment(string_view language) const
{
    try {
        auto const& xml = m_p->xml();

        auto result = xml.comment(language);

        if (result.empty()) {
            return xml.comment("");
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<cstring_view> mime_type::
aliases() const
{
    try {
        auto const& xml = m_p->xml();

        return xml.aliases();
    }
    catch (...) {
        rethrow_error();
    }
}

array<mime_type> mime_type::
base_classes() const
{
    try {
        array<mime_type> result;

        for (auto const& dir: m_p->dirs()) {
            for (auto const& base: dir.subclasses().search(name())) {
                auto it = std::ranges::find_if(result,
                    [&](auto&& mt) { return mt.name() == base; } );

                if (it == result.end()) {
                    emplace_back(result, m_p->db(), base);
                }
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

bool mime_type::
is_subclass_of(string_view base) const noexcept
{
    for (auto const& dir: m_p->dirs()) {
        auto const& subclasses = dir.subclasses();
        auto const& parents = subclasses.search(this->name());

        if (std::ranges::find(parents, base) != parents.end()) {
            return true;
        }
    }

    return false;
}

static string
default_icon_name(string_view name)
{
    try {
        string result { name };

        str::find_replace_all(result, "/", "-");

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

string mime_type::
icon_name() const
{
    using std::ranges::views::reverse;

    try {
        for (auto const& dir: reverse(m_p->dirs())) {
            auto const& o_icon = dir.icons().search(name());
            if (o_icon) {
                return { o_icon->name() };
            }
        }

        return default_icon_name(name());
    }
    catch (...) {
        rethrow_error();
    }
}

static string
default_generic_icon_name(string_view name)
{
    try {
        string result;

        auto [f, _] = str::find_first(name, '/');
        string_view prefix { name.begin(), f };
        result.append(prefix);
        result.append("-x-generic");

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

string mime_type::
generic_icon_name() const
{
    using std::ranges::views::reverse;

    try {
        for (auto const& dir: reverse(m_p->dirs())) {
            auto const& o_icon = dir.generic_icons().search(name());
            if (o_icon) {
                return { o_icon->name() };
            }
        }

        return default_generic_icon_name(name());
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view mime_type::
acronym() const
{
    try {
        auto const& xml = m_p->xml();

        return xml.acronym("");
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view mime_type::
acronym(string_view lang) const
{
    try {
        auto const& xml = m_p->xml();

        auto result = xml.acronym(lang);
        if (!lang.empty() && result.empty()) {
            return xml.acronym("");
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view mime_type::
expanded_acronym() const
{
    try {
        auto const& xml = m_p->xml();

        return xml.expanded_acronym("");
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view mime_type::
expanded_acronym(string_view lang) const
{
    try {
        auto const& xml = m_p->xml();

        auto result = xml.expanded_acronym(lang);
        if (!lang.empty() && result.empty()) {
            return xml.expanded_acronym("");
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<cstring_view> mime_type::
glob_patterns() const
{
    try {
        array<cstring_view> result;

        auto const& xml = m_p->xml();

        for (auto const& glob: xml.globs()) {
            push_back(result, glob.pattern);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

std::ostream&
operator<<(std::ostream& os, mime_type const& m)
{
    try {
        return os << m.name();
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime
