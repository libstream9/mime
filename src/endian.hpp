#ifndef STREAM9_STRINGS_SRC_MIME_ENDIAN_HPP
#define STREAM9_STRINGS_SRC_MIME_ENDIAN_HPP

#include <boost/endian/arithmetic.hpp>

namespace stream9::xdg::mime {

using card8_t = boost::endian::big_uint8_t;
using card16_t = boost::endian::big_uint16_t;
using card24_t = boost::endian::big_uint24_t;
using card32_t = boost::endian::big_uint32_t;

} // namespace stream9::xdg::mime

#endif // STREAM9_STRINGS_SRC_MIME_ENDIAN_HPP
