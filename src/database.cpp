#include <stream9/xdg/mime/database.hpp>

#include <stream9/xdg/mime/environment.hpp>
#include <stream9/xdg/mime/error.hpp>
#include <stream9/xdg/mime/namespace.hpp>

#include "aliases.hpp"
#include "database_impl.hpp"
#include "directory.hpp"
#include "glob_match.hpp"
#include "magic.hpp"
#include "magic_match.hpp"
#include "treemagic_match.hpp"

#include <algorithm>

#include <stream9/array.hpp>
#include <stream9/container/ref_array.hpp>
#include <stream9/filesystem.hpp>
#include <stream9/path/basename.hpp>
#include <stream9/path/concat.hpp>
#include <stream9/xdg/base_dir.hpp>

namespace stream9::xdg::mime {

static void
keep_matches_with_biggest_weight(auto& matches) noexcept
{
    using std::ranges::max;
    using std::ranges::remove_if;

    auto max_weight = max(
        matches, {}, [](auto&& m) { return m.weight(); } ).weight();

    auto [it, end] = remove_if(matches,
        [&](auto&& m) { return m.weight() != max_weight; } );
    matches.erase(it, end);
}

static void
keep_matches_with_longest_pattern(auto& matches) noexcept
{
    using std::ranges::max;
    using std::ranges::remove_if;

    auto max_length = max(
            matches, {}, [](auto&& m) { return m.pattern().size(); }
        ).pattern().size();

    auto [it, end] = remove_if(matches,
        [&](auto&& m) { return m.pattern().size() != max_length; } );
    matches.erase(it, end);
}

static bool
is_all_resolve_to_same_mime_type(auto const& matches) noexcept
{
    using std::ranges::views::drop;

    assert(matches.size() > 1);

    auto mime_type = matches.front().mime_type();
    for (auto const& m: drop(matches, 1)) {
        if (m.mime_type() != mime_type) {
            return false;
        }
    }

    return true;
}

static glob_match const*
definitive_match(array<glob_match>& matches)
{
    try {
        if (matches.empty()) return nullptr;

        if (matches.size() == 1) {
            return &matches.front();
        }

        stream9::container::ref_array candidate = matches;

        keep_matches_with_biggest_weight(candidate);
        assert(!candidate.empty());
        if (candidate.size() == 1) {
            return &candidate.front();
        }

        keep_matches_with_longest_pattern(candidate);
        assert(!candidate.empty());
        if (candidate.size() == 1) {
            return &candidate.front();
        }

        if (is_all_resolve_to_same_mime_type(candidate)) {
            return &candidate.front();
        }
        else {
            return nullptr;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static void
scan_directory(database_impl& p)
{
    using path::operator/;

    try {
        for (auto const& dir: xdg::data_dirs()) {
            p.append_directory(dir / "mime");
        }

        p.append_directory(xdg::data_home() / "mime");
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * database
 */
database::
database()
    try : m_p { *this }
{
    scan_directory(*m_p);
}
catch (...) {
    rethrow_error();
}

database::~database() = default;

opt<mime_type> database::
find_mime_type_for_name(string_view name) const
{
    try {
        for (auto const& dir: m_p->dirs()) {
            if (dir.has_entry(name)) {
                return mime_type { *this, name };
            }
        }

        for (auto const& dir: m_p->dirs()) {
            auto const& aliases = dir.aliases();
            auto unaliased = aliases.search(name);
            if (unaliased) {
                return mime_type { *this, *unaliased };
            }
        }

        return null;
    }
    catch (...) {
        rethrow_error();
    }
}

mime_type database::
find_mime_type_for_file(cstring_ptr const& path) const
{
    try {
        auto glob_matches = m_p->glob_search(path::basename(path));

        if (auto const& o_match = definitive_match(glob_matches); o_match) {
            return { *this, o_match->mime_type() };
        }

        array<char> buf;
        env().load_file(path, buf);

        auto magic_matches = m_p->magic_search(buf);

        return m_p->select_match(glob_matches, magic_matches, buf);
    }
    catch (...) {
        rethrow_error();
    }
}

mime_type database::
find_mime_type_for_file(
    string_view filename, file_data const& data) const
{
    try {
        auto glob_matches = m_p->glob_search(filename);

        if (auto const& o_match = definitive_match(glob_matches); o_match) {
            return { *this, o_match->mime_type() };
        }

        auto magic_matches = m_p->magic_search(data);

        return m_p->select_match(glob_matches, magic_matches, data);
    }
    catch (...) {
        rethrow_error();
    }
}

mime_type database::
find_mime_type_for_filename(string_view filename) const
{
    try {
        auto glob_matches = m_p->glob_search(filename);

        if (auto const& o_match = definitive_match(glob_matches); o_match) {
            return { *this, o_match->mime_type() };
        }
        else {
            return m_p->default_mime_type();
        }
    }
    catch (...) {
        rethrow_error();
    }
}

mime_type database::
find_mime_type_for_data(file_data const& data) const
{
    using std::ranges::sort;
    using std::ranges::greater;

    try {
        auto matches = m_p->magic_search(data);

        if (matches.empty()) {
            return m_p->default_mime_type_for_data(data);
        }
        else {
            sort(matches, greater());
            auto const& m = matches.front();
            return { *this, m.mime_type() };
        }
    }
    catch (...) {
        rethrow_error();
    }
}

database::mime_type_set database::
find_mime_types_for_filename(string_view filename) const
{
    try {
        mime_type_set result;

        auto matches = m_p->glob_search(filename);

        if (matches.empty()) {
            result.insert(m_p->default_mime_type());
        }
        else {
            for (auto const& match: matches) {
                result.insert({ *this, match.mime_type() });
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

database::mime_type_set database::
find_mime_types_for_data(file_data const& data) const
{
    try {
        mime_type_set result;

        auto matches = m_p->magic_search(data);

        if (matches.empty()) {
            result.insert(m_p->default_mime_type_for_data(data));
        }
        else {
            for (auto const& match: matches) {
                result.insert({ *this, match.mime_type() });
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

database::mime_type_set database::
find_mime_types_for_filesystem(cstring_ptr const& root_path) const
{
    try {
        mime_type_set result;

        auto const& matches = m_p->filesystem_search(root_path);

        for (auto const& match: matches) {
            result.insert({ *this, match.mime_type() });
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

database::mime_type_set database::
find_mime_type_for_xml(string_view xml) const
{
    try {
        return m_p->xml_search(xml);
    }
    catch (...) {
        rethrow_error();
    }
}

database::mime_type_set database::
find_mime_type_for_xml_namespace(string_view namespace_uri,
                                 string_view local_name) const
{
    try {
        return m_p->xml_search(namespace_uri, local_name);
    }
    catch (...) {
        rethrow_error();
    }
}

database::mime_type_set database::
find_mime_type_for_non_regular_file(cstring_ptr const& path) const
{
    try {
        mime_type_set result;
        opt<mime_type> o_mt;

        auto is_symlink = env().is_symbolic_link(path);
        if (is_symlink) {
            o_mt = find_mime_type_for_name("inode/symlink");
            if (o_mt) result.insert(std::move(*o_mt));
        }

        try {
            auto type = env().file_type(path);

            switch (type) {
                using enum environment::file_type_t;
                case regular:
                    break;
                case directory:
                    if (!is_symlink && env().is_mount_point(path)) {
                        o_mt = find_mime_type_for_name("inode/mount-point");
                        if (o_mt) result.insert(std::move(*o_mt));
                    }

                    o_mt = find_mime_type_for_name("inode/directory");
                    if (o_mt) result.insert(std::move(*o_mt));
                    break;
                case block_device:
                    o_mt = find_mime_type_for_name("inode/blockdevice");
                    if (o_mt) result.insert(std::move(*o_mt));
                    break;
                case character_device:
                    o_mt = find_mime_type_for_name("inode/chardevice");
                    if (o_mt) result.insert(std::move(*o_mt));
                    break;
                case fifo:
                    o_mt = find_mime_type_for_name("inode/fifo");
                    if (o_mt) result.insert(std::move(*o_mt));
                    break;
                case socket:
                    o_mt = find_mime_type_for_name("inode/socket");
                    if (o_mt) result.insert(std::move(*o_mt));
                    break;
            }
        }
        catch (...) {
            if (result.empty()) rethrow_error();
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

std::uint32_t database::
max_extent() const noexcept
{
    return m_p->get_max_extent();
}

} // namespace stream9::xdg::mime
