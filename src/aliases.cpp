#include "aliases.hpp"

#include <algorithm>

namespace stream9::xdg::mime {

opt<cstring_view> aliases::
search(string_view const alias) const noexcept
{
    using std::ranges::lower_bound;

    opt<cstring_view> result;

    auto const end = this->end();
    auto proj = [](alias_entry const& e) { return e.alias(); };

    auto it = lower_bound(begin(), end, alias, {}, proj);
    if (it != end) {
        auto const& e = *it;
        if (!(alias < e.alias())) {
            result.emplace(e.mime_type());
        }
    }

    return result;
}

} // namespace stream9::xdg::mime
