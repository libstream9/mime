#include "glob_match.hpp"

#include <stream9/xdg/mime/mime_type.hpp>

#include "directory.hpp"

#include <ostream>

namespace stream9::xdg::mime {

bool glob_match::
has_deleteall() const noexcept
{
    return directory().has_glob_deleteall(mime_type());
}

std::strong_ordering glob_match::
operator<=>(glob_match const& o) const noexcept
{
    if (auto cmp = weight() <=> o.weight(); cmp != 0) {
        return cmp;
    }
    else if (auto cmp = pattern().size() <=> o.pattern().size(); cmp != 0) {
        return cmp;
    }
    else {
        return directory() <=> o.directory();
    }
}

std::ostream&
operator<<(std::ostream& os, glob_match const& g)
{
    try {
        os << g.weight() << ":" << g.mime_type() << ":" << g.pattern();
        if (g.flags() & glob_flag::case_sensitive) {
            os << ":cs";
        }

        return os;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::mime
