#include "subclasses.hpp"

#include <algorithm>

namespace stream9::xdg::mime {

parents subclasses::
search(string_view mime_type) const noexcept
{
    auto proj = [](subclass const& s) {
        return s.mime_type();
    };

    auto end = this->end();
    auto it = std::ranges::lower_bound(begin(), end, mime_type, {}, proj);
    if (it == end) {
        return {};
    }
    else {
        auto const& s = *it;
        if (!(mime_type < s.mime_type())) {
            return (*it).parents();
        }
        else {
            return {};
        }
    }
}

} // namespace stream9::xdg::mime
