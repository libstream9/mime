#ifndef STREAM9_XDG_MIME_RAPIDXML_UTIL_HPP
#define STREAM9_XDG_MIME_RAPIDXML_UTIL_HPP

#include <stream9/xdg/mime/error.hpp>
#include <stream9/xdg/mime/namespace.hpp>

#include "rapidxml/rapidxml.hpp"

#include <iterator>

#include <stream9/iterators.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::mime {

class element_iterator : public iter::iterator_facade<
                                            element_iterator,
                                            std::forward_iterator_tag,
                                            rapidxml::xml_node<>& >
{
public:
    element_iterator() = default;
    element_iterator(rapidxml::xml_node<>& parent, string_view name) noexcept
        : m_name { name }
    {
        m_node = parent.first_node(m_name.data(), m_name.size());
    }

private:
    friend class iter::iterator_core_access;

    rapidxml::xml_node<>& dereference() const noexcept { return *m_node; }

    void increment()
    {
        try {
            while (m_node) {
                m_node = m_node->next_sibling(m_name.data(), m_name.size());
                if (m_node && m_node->type() == rapidxml::node_element) {
                    break;
                }
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    bool equal(element_iterator const& other) const noexcept
    {
        return m_node == other.m_node;
    }

    bool equal(std::default_sentinel_t) const noexcept
    {
        return m_node == nullptr;
    }

private:
    rapidxml::xml_node<>* m_node = nullptr;
    string_view m_name;
};

inline auto begin(element_iterator const& it) noexcept { return it; }
inline auto end(element_iterator const&) noexcept { return std::default_sentinel; }

inline cstring_view
name(rapidxml::xml_base<> const& node)
{
    try {
        return { node.name(), node.name_size() };
    }
    catch (...) {
        rethrow_error();
    }
}

inline cstring_view
value(rapidxml::xml_base<> const& node)
{
    try {
        return { node.value(), node.value_size() };
    }
    catch (...) {
        rethrow_error();
    }
}

inline void
set_value(rapidxml::xml_base<>& node, string_view s)
{
    try {
        node.value(s.data(), s.size());
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view
    get_attribute(rapidxml::xml_node<> const&, string_view name);

void set_attribute(rapidxml::memory_pool<>& mp, rapidxml::xml_node<>& node,
                   string_view name, string_view value);

cstring_view
    allocate_string(rapidxml::memory_pool<>& mp, string_view str);

rapidxml::xml_node<>&
    create_element(rapidxml::memory_pool<>&,
                   string_view name, string_view value);

inline rapidxml::xml_node<>&
create_element(rapidxml::memory_pool<>& p, string_view name)
{
    return create_element(p, name, "");
}

rapidxml::xml_attribute<>&
    create_attribute(rapidxml::memory_pool<>&,
                     string_view name, string_view value);

inline void
append_child(rapidxml::xml_node<>& parent, rapidxml::xml_node<>& child)
{
    try {
        parent.append_node(&child);
    }
    catch (...) {
        rethrow_error();
    }
}

inline rapidxml::xml_node<>*
next_sibling(rapidxml::xml_node<> const& node, string_view name)
{
    try {
        return node.next_sibling(name.data(), name.size());
    }
    catch (...) {
        rethrow_error();
    }
}

inline rapidxml::xml_node<>*
next_sibling(rapidxml::xml_node<> const& node)
{
    try {
        return node.next_sibling("", 0);
    }
    catch (...) {
        rethrow_error();
    }
}

rapidxml::xml_node<>*
    first_element(rapidxml::xml_node<> const& parent, string_view name);

rapidxml::xml_node<>*
    first_element(rapidxml::xml_node<>& parent,
                  string_view name,
                  string_view attr_name,
                  string_view attr_value);

inline auto*
first_attribute(rapidxml::xml_node<> const& node, string_view name)
{
    try {
        return node.first_attribute(name.data(), name.size());
    }
    catch (...) {
        rethrow_error();
    }
}

cstring_view
    get_text(rapidxml::xml_node<>& parent,
         string_view element, string_view language);

void set_text(rapidxml::memory_pool<>& mp, rapidxml::xml_node<>& parent,
         string_view element, string_view language,
         string_view text);

rapidxml::xml_node<>& root_element(rapidxml::xml_document<>& doc);

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_RAPIDXML_UTIL_HPP
