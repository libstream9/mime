#ifndef STREAM9_XDG_MIME_GLOBS_HPP
#define STREAM9_XDG_MIME_GLOBS_HPP

#include <stream9/xdg/mime/namespace.hpp>

#include "endian.hpp"

#include "cache.hpp"
#include "glob_flag.hpp"
#include "glob_record.hpp"
#include "glob_weight.hpp"

#include <compare>
#include <cstdint>
#include <iosfwd>
#include <iterator>
#include <ranges>

#include <stream9/array.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/iterators.hpp>
#include <stream9/ranges/range_facade.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::mime {

class glob_iterator;

struct glob_entry;
struct glob_list;

class glob
{
public:
    glob(char const* base, glob_entry const*) noexcept;

    cstring_view pattern() const noexcept;
    cstring_view mime_type() const noexcept;
    glob_flags flags() const noexcept;
    glob_weight weight() const noexcept;

    template<std::size_t I>
        requires (I < 4)
    decltype(auto) get() const noexcept;

private:
    char const* m_base; // non-null
    glob_entry const* m_entry; // non-null
};

class globs : public stream9::ranges::range_facade<globs>
{
public:
    using const_iterator = glob_iterator;

public:
    globs(class cache const&, char const* base, card32_t offset) noexcept;

    // accessor
    class cache const& cache() const noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    // query
    std::uint32_t size() const noexcept;

    void literal_search(array<glob_record>& result,
                        string_view filename) const;

    void fnmatch_search(array<glob_record>& result,
                        cstring_ptr const& filename) const;

    bool has_match(string_view mime_type,
                   string_view pattern) const noexcept;

private:
    class cache const* m_cache; // non-null
    char const* m_base; // non-null
    glob_list const* m_record; // non-null
};

/**
 * @model std::random_access_iterator
 */
class glob_iterator : public iter::iterator_facade<glob_iterator,
                                            std::random_access_iterator_tag,
                                            glob,
                                            glob >
{
public:
    glob_iterator() = default;
    glob_iterator(char const* base, glob_entry const*) noexcept;

private:
    friend class iter::iterator_core_access;

    glob dereference() const noexcept;

    void increment() noexcept;

    void decrement() noexcept;

    void advance(difference_type) noexcept;

    difference_type distance_to(glob_iterator const&) const noexcept;

    bool equal(glob_iterator const&) const noexcept;

    std::strong_ordering compare(glob_iterator const&) const noexcept;

private:
    char const* m_base = nullptr;
    glob_entry const* m_entry = nullptr;
};

std::ostream& operator<<(std::ostream&, glob const&);
std::ostream& operator<<(std::ostream&, globs const&);

} // namespace stream9::xdg::mime

#include "globs.tcc"

namespace std::ranges {

template<>
inline constexpr bool enable_borrowed_range<stream9::xdg::mime::globs> = true;

} // namespace std::ranges

namespace std {

template<>
struct tuple_size<stream9::xdg::mime::glob>
    : integral_constant<size_t, 4> {};

template<size_t I>
    requires (I == 0 || I == 1)
struct tuple_element<I, stream9::xdg::mime::glob>
{
    using type = char const*;
};

template<size_t I>
    requires (I == 2 || I == 3)
struct tuple_element<I, stream9::xdg::mime::glob>
{
    using type = uint32_t;
};

} // namespace std

#endif // STREAM9_XDG_MIME_GLOBS_HPP
