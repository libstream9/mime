#ifndef STREAM9_XDG_MIME_MAGIC_MATCH_TCC
#define STREAM9_XDG_MIME_MAGIC_MATCH_TCC

#include "magic_match.hpp"

#include <stream9/xdg/mime/mime_type.hpp>

namespace stream9::xdg::mime {

inline magic_match::
magic_match(class directory const& dir, class magic const& m) noexcept
    : m_dir { &dir }
    , m_magic { m }
{}

inline class directory const& magic_match::
directory() const noexcept
{
    return *m_dir;
}

inline class magic const& magic_match::
magic() const noexcept
{
    return m_magic;
}

inline cstring_view magic_match::
mime_type() const noexcept
{
    return m_magic.mime_type();
}

inline magic_priority magic_match::
priority() const noexcept
{
    return { m_magic.priority() };
}

inline bool magic_match::
operator==(magic_match const& o) const noexcept
{
    return &m_magic == &o.m_magic;
}

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_MAGIC_MATCH_TCC
