#ifndef STREAM9_XDG_MIME_FILE_DATA_HPP
#define STREAM9_XDG_MIME_FILE_DATA_HPP

#include <stream9/array_view.hpp>

namespace stream9::xdg::mime {

using file_data = array_view<char const>;

} // namespace stream9::xdg::mime

#endif // STREAM9_XDG_MIME_FILE_DATA_HPP
